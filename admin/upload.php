<?php include("includes/header.php"); ?>

<?php if(!$session->is_signed_in()) {redirect("login.php"); } ?>

<?php

    $message = "";
    // if(isset($_POST['submit'])) {
    //     // echo "<h1> IT works </h1>"; //test works or not
    //     $photo = new Photo();
    //     $photo->title = $_POST['title'];
    //     $photo->set_files($_FILES['file_upload']);

    //     if($photo->save()) {

    //         $message = "upload successfully";
    //         $class = "bg-success";
        
    //     } else {
        
    //         //$session->message(join("<br>", $photo->errors));
    //         $class = "bg-warning";
    //         $message = join("<br>", $photo->errors);
            
        
    //     }

    // }

    if(isset($_FILES['file'])) {
        // echo "<h1> IT works </h1>"; //test works or not
        $photo = new Photo();
        $photo->title = $_POST['title'];
        $photo->set_files($_FILES['file']);

        if($photo->save()) {

            $message = "upload successfully";
            $class = "bg-success";
        
        } else {
        
            //$session->message(join("<br>", $photo->errors));
            $class = "bg-warning";
            $message = join("<br>", $photo->errors);
            
        
        }

    }


?>

<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->

    <?php include("includes/top_nav.php"); ?>

    <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
    <?php include("includes/side_nav.php"); ?>

    <!-- /.navbar-collapse -->
</nav>

<div id="page-wrapper">
    <div class="container-fluid">
        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Upload
                    <small></small>
                </h1>
                <div class="row">
                    <p class="<?php echo $class ?>"><?php echo $message;?></p>
                    <div class="col-md-6">
                        <form action="upload.php" method="POST" enctype="multipart/form-data">
                            <div class="form-group">
                                <input type="text" name="title" class="form-control" >
                            </div>
                            <div class="form-group">
                                <input type="file" name="file" >
                            </div>
                            <input type="submit" name="submit">
                        </form>
                    </div>
                </div> <br>
                <div class="row">
                    <div class="col-lg-12">
                        <form action="upload.php" class="dropzone"></form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->

    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

<?php include("includes/footer.php"); ?>