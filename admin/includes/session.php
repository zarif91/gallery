<?php

class Session {

    private $signed_in = false;
    public $user_id;
    public $message;
    public $count;

    function __construct() {
        session_start(); //google it
        $this->visitor_count();
        $this->check_the_login();
        $this->check_message();
    }

    // Getter
    public function is_signed_in() {
        return $this->signed_in;
    }

    // Login the user
    public function login($user) {
        if($user) {
            $this->user_id = $_SESSION['user_id'] = $user->id;
            $this->signed_in = true;
        }
    }

    //Logout the user
    public function logout() {
        unset($_SESSION['user_id']);
        unset($this->user_id);
        $this->signed_in = false;
    }

    //Check login or not
    private function check_the_login() {
        if(isset($_SESSION['user_id'])){
            $this->user_id = $_SESSION['user_id'];
            $this->signed_in = true;
        } else {
            unset($this->user_id); //clear the user_id
            $this->signed_in = false;
        }
    }

    //Display message
    public function message($msg="") {
        if(!empty($msg)) {
            $_SESSION['message'] = $msg;
        } else {
            return $this->message;
        }
    }

     //Check message
     public function check_message() {
        if(isset($_SESSION['message'])){
            $this->message = $_SESSION['message'];
            unset($_SESSION['message']);
        } else {
            $this->message = "";
        }
    }

    public function visitor_count() {

        if(isset($_SESSION['count'])) {

            return $this->count = $_SESSION['count']++;

        } else {

            return $_SESSION['count'] = 1;

        }

    }
}

$session = new Session();
$message = $session->message();
