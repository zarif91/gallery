$(document).ready(function(){

    var user_href;
    var user_href_splitted;
    var user_id;

    var image_src;
    var image_src_splitted;
    var image_filename;

    var photo_id;

    $(".modal_thumbnails").click(function(){

        $("#set_user_image").prop('disabled', false);

        //* get user id
        user_href = $("#user-id").prop('href');
        user_href_splitted = user_href.split("=");
        user_id = user_href_splitted[user_href_splitted.length-1];

        //* get image name
        image_src = $(this).prop("src");
        image_src_splitted = image_src.split("/");
        image_filename = image_src_splitted[image_src_splitted.length-1];

        photo_id = $(this).attr("data");
        
        $.ajax({

            url: "includes/ajax_code.php",
            data: {photo_id: photo_id},
            type: "POST",
            success: function(data){

                if(!data.error) {

                    $("#modal_sidebar").html(data);

                }
            }

        });
        

    });

    $("#set_user_image").click(function(){

        $.ajax({

            url: "includes/ajax_code.php",
            data: {image_filename: image_filename, user_id: user_id},
            type: "POST",
            success: function(data){

                if(!data.error) {

                    $(".user_image_box a img").prop('src', data);               
                }
            }
        });
    });


    /*************** Edit Photo Side Bar *****************/

    $(".info-box-header").click(function(){

        $(".inside").slideToggle("fast");

        $("#toggle").toggleClass("glyphicon glyphicon-menu-down , glyphicon glyphicon-menu-up");

    });


    /*************** Delete functions *****************/
    $(".delete_link").click(function(){

        return confirm("Are you sure you want to delete this item? ");
        
    });

    tinymce.init({selector:'textarea'});

});
